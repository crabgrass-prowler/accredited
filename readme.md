# Accredited

This is pretty cheeky, it scrapes a page from immigration nz to extract a json object which holds _all_ the accredited employers in NZ.

Then it will construct a query for Seek.co.nz and try to find matching jobs with that company + 'C#' in Auckland.

## Python stuff

* Use a python virtual environment. [See here](https://docs.python.org/3/library/venv.html)
* Use pip + requirements file to install packages [See here](https://pip.pypa.io/en/stable/user_guide/#requirements-files)

That will look a lot like this (maybe a little different on windows)

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

### Running

```bash
python accredited.py
```

This will write a file into `results/` for today's date.
