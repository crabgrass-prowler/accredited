from accredited import AccreditedEmployer, scrape_immigration_nz

from pytest_mock import MockerFixture


def test_accredited_employer_clean_company_name():
    ae = AccreditedEmployer(
        id="abc",
        employer_name="Comma, llc Comma. (ltd) Limited, ltd. limited lLC.. :)",
    )

    assert ae.clean_company_name() == "comma comma"


def test_scrape_immigration_nz(mocker: MockerFixture):
    desired_json = """
    var accredited_employers_registry_data =
    [
        {"lol": "kek"}
    ];
    """
    long_response = f"""
    <html>
    lol, here we go
    <script>
    var kek = [];
    {desired_json}
    var lol = [];
    </script>
    """

    m = mocker.patch("accredited.requests.get")
    m.return_value.text = long_response

    assert scrape_immigration_nz() == [{"lol": "kek"}]
