import json
import regex
import time
from dataclasses import dataclass, asdict
from datetime import date
from typing import Dict, List
from urllib.parse import quote

import requests
from tqdm import tqdm


def main():
    employers = scrape_immigration_nz()

    for employer in tqdm(employers, leave=False):
        try:
            employer.number_of_hits = get_count_of_results_from_seek(
                employer.clean_company_name()
            )
        except requests.exceptions.RequestException:
            print(f"\n\rBreaking at {employer=}", flush=True)
            break
        if employer.number_of_hits > 0:
            print(f"\r{employer}", flush=True)

    employers_with_hits: List[Dict] = [
        asdict(e) for e in employers if e.number_of_hits > 1
    ]
    with open(f"results/{date.today()}.json", "w") as f:
        json.dump(employers_with_hits, fp=f, indent=4)


@dataclass
class AccreditedEmployer:
    """
    Class that represents an accredited employer, and can track a number of hits.
    """

    id: str
    employer_name: str
    url: str = ""
    number_of_hits: int = 0

    def __str__(self):
        return f"{self.employer_name}: {self.number_of_hits:>5}"

    def clean_company_name(self):
        """
        return a version of the company name thats better suited to searching with.
        """
        cleaned_name = regex.sub(r"[^a-zA-Z\s]", "", self.employer_name).lower()

        for word in ("ltd", "limited", "llc"):
            cleaned_name = cleaned_name.replace(word, "")

        return regex.sub(r"\s+", " ", cleaned_name).strip()


def scrape_immigration_nz() -> List[AccreditedEmployer]:
    response = requests.get(
        "https://www.immigration.govt.nz/new-zealand-visas/apply-for-a-visa/"
        "tools-and-information/tools/accredited-employers-list"
    )
    lines = iter(response.text.splitlines())
    json_lines: List[str] = []

    for line in lines:
        # Look until we find the start of the json blob
        if "var accredited_employers_registry_data" in line:
            break
    for line in lines:
        # Look for the end of the blob
        if line.strip() == "];":
            json_lines.append("]")
            break
        json_lines.append(line)

    employers: List[AccreditedEmployer] = []
    for employer in json.loads("\n".join(json_lines)):
        id_field = employer["id"]
        employer_name = employer["title"]
        try:
            url = employer["secondary_data"][1]["url"]
        except (IndexError, KeyError):
            url = ""
        employers.append(
            AccreditedEmployer(id=id_field, employer_name=employer_name, url=url)
        )
    return employers


def get_count_of_results_from_seek(employer_name: str) -> int:
    city = "Auckland"
    language = "C#"  # C# in urlencoded format
    url = (
        "https://jobsearch-api.cloud.seek.com.au/counts?siteKey=NZ-Main"
        f"&sourcesystem=houston&where={quote(city)}&page=1&seekSelectAllPages=true&"
        f"keywords={quote(employer_name)}+{quote(language)}"
    )

    time.sleep(0.1)  # Plz no ban me
    r = requests.get(url)

    # '1018' is a magic number that correlates 1-to-1 with the number of total results
    #  shown on the page when I do the same searches as a user
    return r.json()["counts"][0]["items"].get("1018", 0)


if __name__ == "__main__":
    main()
