The immigration NZ response format looks like:

```json
[
    ...
    {
        "id": "drillco-2019-limited",
        "secondary_data": [
            {
                "text": "Construction & Building",
                "type": "text"
            },
            {
                "text": "www.drillco.co.nz",
                "type": "link",
                "url": "http://www.drillco.co.nz"
            }
        ],
        "title": "Drillco 2019 Limited"
    },
    ...
]
```
